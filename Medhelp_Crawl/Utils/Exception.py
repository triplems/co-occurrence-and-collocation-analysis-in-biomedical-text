class ScrapingError(Exception):
    pass

class InvalidCommunityPageError(ScrapingError):
    
    def __init__(self, 
                 message: str,
                 max_pages: int = None,
                 questions_data: dict = {},
                 users_data: dict = {}
                 ) -> None:
        super().__init__(message)
        
        self.max_page = max_pages
        self.questions_data = questions_data
        self.users_data = users_data
    
    def __str__(self) -> str:
        if self.max_page is not None:
            return f"{self.message} (Maximum Pages: {self.max_page})"
        return self.message