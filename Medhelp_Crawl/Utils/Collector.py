class VariableCollector:
    def __init__(self, *args, **kwargs) -> None:
        self.vars = dict()
        
        for idx, arg in enumerate(args):
            self.vars[f"arg_{idx + 1}"] = arg
            
        self.vars.update(kwargs)
        
    def get_variables(self) -> dict:
        return self.vars
