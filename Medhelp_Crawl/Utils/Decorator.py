import time


def timing_method(func):

    def wrapper(*args, **kwargs):
        func_name = (
            args[0].__class__.__module__ + "." +
            args[0].__class__.__name__ 
        ) if args else "" + func.__name__
        print("Starting " + func_name + " ...")
        ts = time.time()
        result = func(*args, **kwargs)
        te = time.time()
        print("func:%r args:[%r] took: %2.4f sec" %
              (func.__name__, kwargs, te-ts)
              )
        print("End of " + func_name + " ...")
        return result

    return wrapper

    
