from abc import ABCMeta, abstractmethod
from Crawler.Communities_Crawler import CommunitiesCrwaler
from Crawler.Conversations_Crawler import ConversationsCrawler
from Crawler.Posts_crawler import PostsCrawler
from Parser.Utils.DB.PgSQL import PostgreSQLConnector
from Utils.Decorator import timing_method
from configparser import ConfigParser
from typing import Dict, Union
import time, os

CONST_CONFIG = ConfigParser(delimiters=("="))
CONST_CONFIG.read(
    os.path.join(
        os.path.dirname(__file__),
        "Constants.ini"
    )
)

# Interface for different modes
class CrawlerMode(metaclass=ABCMeta):
    """Base class for different crawler operation modes."""
    
    def __init__(self) -> None:
        super().__init__()

    @abstractmethod
    def run(self) -> None:
        """Abstract method to implement the specific operation of the mode."""
        pass

class ManualMode(CrawlerMode):
    """Mode for manually triggering different crawler operations."""

    @timing_method
    def run(self) -> None:
        try:
            # CommunitiesCrwaler(base_url=CONST_CONFIG["Request"]["base_url"],
            #                    url_path=CONST_CONFIG["Communities"]["url_path"]
            #                    ).crawl()
            
            # ConversationsCrawler(base_url=CONST_CONFIG["Request"]["base_url"],
            #                      community_link=CONST_CONFIG["Conversations"]["community_link"],
            #                      community_name=CONST_CONFIG["Conversations"]["community_name"]
            #                      ).crawl()
            
            for idx, conversation_link in enumerate(get_conversation_links("C_026")[78:]):
                if not (idx % 10):
                    time.sleep(10)
                print(f"==================================================================={idx}")
                PostsCrawler(base_url=CONST_CONFIG["Request"]["base_url"],
                            # conversation_link=CONST_CONFIG["Posts"]["conversation_link"]
                            conversation_link = conversation_link
                            ).crawl()
                
            
        except Exception as ex:
            ex.args = (*ex.args, "Error Occurred During Running Manual Mode")
            raise ex
        
class DailyMode(CrawlerMode):
    """Mode for running daily scheduled crawler operations."""

    @timing_method
    def run(self) -> None:
        print("Starting Daily Mode...")
        
        try:
            conversations = MainCrawler().crawl_conversations()
            print("Conversations retrieved:", len(conversations))
        except Exception as ex:
            ex.args = (*ex.args, "Error Occurred During Running Daily Mode")
            raise ex
        
        print("End of Daily Mode.")
        
        
class ModeManager:
    """Manages execution of different crawler modes."""

    _modes: Dict[str, CrawlerMode]

    def __init__(self) -> None:
        self._modes = {
            "manual": ManualMode(),
            "daily": DailyMode(),
        }

    def run_mode(self, mode_name: str) -> None:
        """Executes the specified crawler mode."""
        
        if mode_name not in self._modes:
            raise ValueError(f"Invalid mode name: '{mode_name}'")

        self._modes[mode_name].run()
        
        
def get_conversation_links(community_id):
    pg_connector = PostgreSQLConnector()
    query  = f"""
        SELECT
            conversation_link
        FROM 
            question
        WHERE
            community_id = '{community_id}'
    """
    pg_connector.execute_query(query)
    conversation_links = [conv_link_tuple[0] for conv_link_tuple in pg_connector.cur.fetchall()]
    pg_connector.close_connection()  
    return conversation_links
