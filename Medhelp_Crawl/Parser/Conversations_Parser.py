from Utils.Decorator import timing_method
from .MHDC_Parser import MainParser

class ConversationsParser(MainParser):
    
    def __init__(self, data: dict) -> None:
        super().__init__(data)

        self.community_id = self.get_community_id(self.data.get("community_link"))
    
    @timing_method
    def parse(self) -> None:
        try:
            self.update_community_max_pages(self.community_id, self.data.get("max_pages"))
            
            # Insert Records Of User Table
            users_query = """
                INSERT INTO "user" (user_id, username, personal_page_link, profile_image_link)
                VALUES ('%(user_id)s', '%(username)s', '%(personal_page_link)s', '%(profile_image_link)s')
                ON CONFLICT ON CONSTRAINT user_un -- on user_id
                DO 
                    UPDATE SET username = 
                        CASE EXCLUDED.username
                            WHEN REGEXP_REPLACE("user".username, '(###).*', '') THEN EXCLUDED.username
                            ELSE EXCLUDED.username || '###' || "user".username
                        END,
                    personal_page_link = EXCLUDED.personal_page_link,
                    profile_image_link = EXCLUDED.profile_image_link;
            """
            users_data_to_insert = []
            for page_no, user_infos in self.data.get("users").items():
                users_data_to_insert.extend(user_infos)
            self.pg_connector.insertion(users_query, users_data_to_insert)
            
            # Insert Records Of Question Table
            questions_query = """
                INSERT INTO question (community_id, questioner_id, subj_id, subj_title, conversation_link, subj_msg, answer_count, comment_count, forum_page_no)
                VALUES ('%(community_id)s', '%(questioner_id)s', %(subj_id)s, '%(subj_title)s', '%(conversation_link)s', '%(subj_msg)s', %(answer_count)s, %(comment_count)s, %(forum_page_no)s)
                ON CONFLICT ON CONSTRAINT question_un -- on (community_id, conversation_link)
                DO 
                    UPDATE SET questioner_id = EXCLUDED.questioner_id,
                    subj_title = EXCLUDED.subj_title,
                    subj_msg = EXCLUDED.subj_msg,
                    answer_count = EXCLUDED.answer_count,
                    comment_count = EXCLUDED.comment_count,
                    forum_page_no = EXCLUDED.forum_page_no;
            """
            questions_data_to_insert = []
            for page_no, question_infos in self.data.get("questions").items():
                questions_data_to_insert.extend(
                    [dict(question_info_dict,
                          **{'community_id': self.community_id}) 
                     for question_info_dict in question_infos]
                )
            self.pg_connector.insertion(questions_query, questions_data_to_insert)
            
        except Exception as ex:
            ex.args = (*ex.args,
                       "Parsing Conversations (Questions And Users)")
            raise ex
        
        finally:
            self.pg_connector.close_connection()
            
    @timing_method
    def parse_community_resources(self) -> None:
        try:
            # Insert Records Of Resource Table
            resources_query = """
                INSERT INTO resource (community_id, article_title, article_link, article_desc, article_img_link)
                VALUES ('%(community_id)s', '%(article_title)s', '%(article_link)s', '%(article_desc)s', '%(article_img_link)s')
                ON CONFLICT ON CONSTRAINT resource_un -- on (community_id, article_link)
                DO 
                    UPDATE SET article_title = EXCLUDED.article_title,
                    article_desc = EXCLUDED.article_desc,
                    article_img_link = EXCLUDED.article_img_link;
            """
            resources_data_to_insert = [dict(resource_dict,
                                             **{'community_id': self.community_id}) 
                                        for resource_dict in self.data.get("resources")]
            self.pg_connector.insertion(resources_query, resources_data_to_insert)
            
        except Exception as ex:
            ex.args = (*ex.args,
                       "Parsing Conversations (Resources)")
            raise ex
        
        finally:
            self.pg_connector.close_connection()    
        
    def get_community_id(self, community_link):
        query  = f"""
            SELECT
                id
            FROM 
                community
            WHERE
                forum_link = '{community_link}'
            LIMIT
                1 
        """
        self.pg_connector.execute_query(query)
        id = self.pg_connector.cur.fetchone()[0]
        return id
    
    def update_community_max_pages(self, community_id, max_pages):
        query = """
            UPDATE
                community
            SET 
                max_pages = %s
            WHERE
                id = %s
        """
        print(query)
        self.pg_connector.execute_query(query, (max_pages, community_id))
        self.pg_connector.commit_changes()
    
    
        