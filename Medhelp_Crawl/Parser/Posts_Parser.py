from Utils.Decorator import timing_method
from .MHDC_Parser import MainParser
from typing import Union

class PostParser(MainParser):
    
    def __init__(self, data: Union[dict, list]) -> None:
        super().__init__(data)

        if type(data) != list and (conv_link := self.data.get("conversation_link")):
            self.question_id = self.get_question_id(conv_link)
    
    @timing_method
    def parse(self) -> None:
        try:
            # Insert Records Of Question Table
            posts_query = """
                INSERT INTO post (subj_id, author_id, post_id, answer_post_id, post_msg, type, created_timestamp, upvote_count, best_answer, url)
                VALUES (%(subj_id)s, '%(author_id)s', %(post_id)s, %(answer_post_id)s, '%(post_msg)s', '%(type)s', %(created_timestamp)s, %(upvote_count)s, %(best_answer)s, '%(url)s')
                ON CONFLICT ON CONSTRAINT post_un -- on (post_id)
                DO 
                    UPDATE SET subj_id = EXCLUDED.subj_id,
                    author_id = EXCLUDED.author_id,
                    answer_post_id = EXCLUDED.answer_post_id,
                    post_msg = EXCLUDED.post_msg,
                    type = EXCLUDED.type,
                    created_timestamp = EXCLUDED.created_timestamp,
                    upvote_count = EXCLUDED.upvote_count,
                    best_answer = EXCLUDED.best_answer,
                    url = EXCLUDED.url;
            """

            self.pg_connector.insertion(posts_query, self.data)
    
        except Exception as ex:
            ex.args = (*ex.args,
                       "Parsing Posts (Answers and Comments)")
            raise ex
        
        finally:
            self.pg_connector.close_connection()
    
    def insert_related_users(self) -> None:
        try:
            related_users_query = """
                INSERT INTO "user" (user_id, username, personal_page_link, profile_image_link)
                VALUES ('%(user_id)s', '%(username)s', '%(personal_page_link)s', '%(profile_image_link)s')
                ON CONFLICT ON CONSTRAINT user_un -- on user_id
                DO NOTHING
            """

            self.pg_connector.insertion(related_users_query, self.data)
            
        except Exception as ex:
            ex.args = (*ex.args,
                       "Inserting related users into User table (No update if exists)")
            raise ex
        
        finally:
            self.pg_connector.close_connection() 
                    
    def insert_related_questions(self) -> None:
        try:
            related_questions_query = """
                INSERT INTO question (community_id, questioner_id, subj_id, subj_title, conversation_link, subj_msg, forum_page_no)
                VALUES ('%(community_id)s', '%(questioner_id)s', %(subj_id)s, '%(subj_title)s', '%(conversation_link)s', '%(subj_msg)s', %(forum_page_no)s)
                ON CONFLICT ON CONSTRAINT question_un -- on (community_id, conversation_link)
                DO NOTHING
            """
            
            related_questions_data_to_insert = [] 
            for _dict in self.data:
                community_id = self.get_community_id_by_forum_name(_dict.get("forum_name"))
                related_questions_data_to_insert.append(
                    dict(_dict.get("question_info"), **{'community_id': community_id}))
                
            """
            TODO:
            - Leverage the "question_un_2" unique constraint on the 'subj_id' attribute to directly
            query the desired data without fetching 'community_id' from the "community" table.
            """

            self.pg_connector.insertion(related_questions_query, related_questions_data_to_insert)
            
        except Exception as ex:
            ex.args = (*ex.args,
                       "Inserting related questions into Question table (No update if exists)")
            raise ex
        
        finally:
            self.pg_connector.close_connection() 
            
    def insert_into_questions_relation(self) -> None:
        try:
            _data = list()
            for related_conversation_link in self.data.get('rel_conv_links'):
                related_question_id = self.get_question_id(related_conversation_link)
                _data.append(
                    {
                        "associative_id": self.question_id,
                        "linked_id": related_question_id,
                    }
                )
            
            query = """
                INSERT INTO "questionsRelation" (associative_id, linked_id)
                VALUES (%(associative_id)s, %(linked_id)s)
                ON CONFLICT ON CONSTRAINT questionsrelation_un -- on (associative_id, linked_id)
                DO NOTHING
            """
            
            self.pg_connector.insertion(query, _data)
            
        except Exception as ex:
            ex.args = (*ex.args,
                       "Inserting relations into QuestionsRelation table")
            raise ex
        
        finally:
            self.pg_connector.close_connection() 
    
    def update_question(self) -> None:
        question_data_dict = self.data.get("question")
        try:
            question_query = """
                UPDATE 
                    question
                SET 
                    subj_msg = %s,
                    created_timestamp = %s
                WHERE
                    conversation_link = %s
            """
            self.pg_connector.execute_query(question_query, 
                                            (question_data_dict["subj_msg"],
                                             question_data_dict["created_timestamp"],
                                             self.data.get("conversation_link"))
                                            )
            self.pg_connector.commit_changes()
            
        except Exception as ex:
            ex.args = (*ex.args,
                       "Updating Question's table fields")
            raise ex
        
        finally:
            self.pg_connector.close_connection()  
    
    def update_user_type(self) -> None:
        user_query = """
            UPDATE
                "user"
            SET 
                type = %s
            WHERE
                user_id = %s
        """
        self.pg_connector.execute_query(user_query, 
                                        (self.data.get("type"),
                                         self.data.get("user_id"))
                                        )
        self.pg_connector.commit_changes()
        self.pg_connector.close_connection()  
            
    def get_question_id(self, conversation_link):
        query  = f"""
            SELECT
                id
            FROM 
                question
            WHERE
                conversation_link = '{conversation_link}'
            LIMIT
                1 
        """
        self.pg_connector.execute_query(query)
        id = self.pg_connector.cur.fetchone()[0]
        return id
    
    def get_community_id_by_forum_name(self, forum_name: str):
        query  = f"""
            SELECT
                id
            FROM 
                community
            WHERE
                forum_name = '{forum_name}'
            LIMIT
                1 
        """
        self.pg_connector.execute_query(query)
        id = self.pg_connector.cur.fetchone()[0]
        return id 