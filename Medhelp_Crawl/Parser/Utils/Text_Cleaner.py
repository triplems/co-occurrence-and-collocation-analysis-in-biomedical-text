from typing import Union
import re

class TextCleaner:
    def __init__(self, text: Union[str, list]) -> None:
        self.text = text

    def strip_whitespace(self) -> str:
        """Strip leading and trailing whitespace."""
        return self.text.strip()
    
    def capitalize(self) -> str:
        return self.text.title()
    
    def apply_filters(self, filters: Union[str, list]) -> str:
        try:
            for method_name in ([filters] if isinstance(filters, str) else filters):
                method = getattr(self, method_name, None)
                if callable(method):
                    self.text = method()
                else:
                    raise AttributeError(f"Method '{method_name}' doesn't exist in {self.__class__.__name__}")
            return self.text
        except Exception as ex:
            ex.args = (*ex.args, "Error Occurred During Applying Filters When Cleaning Text")
            raise ex from None # Preserve original tracebac
        
    def join_strings(self, delimiter: str = " ") -> str:
        string_list = [self.text] if isinstance(self.text, str) else self.text
        return re.sub(r"\s+", " ", delimiter.join(s.strip() for s in string_list))