import psycopg2
from .Config import config


class PostgreSQLConnector:

    def __init__(self) -> None:
        params = config(section="postgresql")
        print('Connecting to the PostgreSQL database...')
        self.conn = psycopg2.connect(**params)
        self.cur = self.conn.cursor()

    def insertion(self, query: str, values: list) -> None:
        try:
            inserted_counter = 0
            for idx, value in enumerate(values):
                try:
                    self.execute_query("SAVEPOINT bulk_savepoint")

                    value = tuple(val.replace("'", "''")
                                  if type(val) == str
                                  else val
                                  for val in value)  \
                        if type(value) in [tuple, list] \
                        else {key: val.replace("'", "''")
                              if type(val) == str
                              else val
                              for key, val in value.items()}

                    self.execute_query((query % value).replace("'None'", "null").replace("None", "null"))
                    inserted_counter += 1

                except psycopg2.errors.UniqueViolation as e:
                    # Roll back to the last commit
                    # self.conn.rollbackto("savepoint")

                    self.execute_query("ROLLBACK TO SAVEPOINT bulk_savepoint")
                    pass

                else:
                    self.execute_query("RELEASE SAVEPOINT bulk_savepoint")

        except psycopg2.Error as ex:
            print(value)
            ex.args = (*ex.args, f"Error Occurred During Inserting To DB")
            raise ex

        finally:
            self.commit_changes()
            print(f"{inserted_counter} rows inserted...")

    def reset_sequence(self, seq_name: str) -> None:
        try:
            query = f"ALTER SEQUENCE {seq_name} RESTART WITH 1;"
            self.execute_query(query)
            self.commit_changes()

        except psycopg2.Error as ex:
            ex.args = (*ex.args, f"Error Occurred During Reseting Sequence")
            raise ex

    def execute_query(self, query: str, query_values: tuple = None) -> ...:
        try:
            self.cur.execute(query) \
                if query_values is None \
                else self.cur.execute(query, query_values)

        except psycopg2.Error as ex:
            print(ex)
            ex.args = (*ex.args, f"Error Occurred During Executing Query")
            raise ex

    def commit_changes(self):
        """Save the changes to the database."""
        try:
            self.conn.commit()
            print("Changes committed successfully...")

        except psycopg2.Error as ex:
            ex.args = (*ex.args, f"Error Occurred Commiting Changes")
            raise ex

    def close_connection(self):
        self.cur.close()
        self.conn.close()
        print("Connection closed...")


# print(PostgreSQLConnector().execute_query("Select Version();"))
