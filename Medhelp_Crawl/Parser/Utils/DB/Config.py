from configparser import ConfigParser
from pathlib import Path
import os 
   
def config(filename: str = "database.ini", section: str = "postgresql") -> dict:
    try:
        parser = ConfigParser(delimiters=("="))
        parser.read(os.path.join(os.path.dirname(__file__), filename))
        
        db = dict()
        if parser.has_section(section):
            params = parser.items(section)
            for param in params:
                db[param[0]] = param[1]
        else:
            raise Exception("Section {0} not found in the {1} file".format(section, filename))
        
        return db
    except Exception as ex:
        ex.args = (*ex.args, f"Error Occurred During Configuring The '{section}' Database")
        raise ex