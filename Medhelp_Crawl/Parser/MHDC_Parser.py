from .Utils.DB.PgSQL import PostgreSQLConnector
from Utils.Decorator import timing_method
from abc import ABCMeta, abstractmethod

class MainParser(metaclass = ABCMeta):

    def __init__(self, data: ...) -> None:
        self.data = data
        self.pg_connector = PostgreSQLConnector()

    @abstractmethod
    def parse(self) -> None:
        print("Start Parsing ...")
        pass
    