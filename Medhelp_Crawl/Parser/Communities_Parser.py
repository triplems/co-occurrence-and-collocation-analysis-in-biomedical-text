from Utils.Decorator import timing_method
from .MHDC_Parser import MainParser

class CommunitiesParser(MainParser):
    
    def __init__(self, data: ...) -> None:
        super().__init__(data)
        
    @timing_method
    def parse(self):
        super().parse()
        
        try:
            data_to_insert = []
            for forum_group_title, community_infos in self.data.items():
                for community_info in community_infos:
                    data_to_insert.append( 
                        (forum_group_title, *community_info.values())
                    )
            print(f"All the taken communities length is {len(data_to_insert)}")
            
            query = """
                INSERT INTO community (forum_group_title, forum_name, forum_link)
                VALUES ('%s', '%s', '%s')
            """

            self.pg_connector.reset_sequence("community_id_seq")
            self.pg_connector.insertion(query, data_to_insert)
            
        except Exception as ex:
            ex.args = (*ex.args,
                       "Parsing Communities")
            raise ex

        finally:
            self.pg_connector.close_connection()