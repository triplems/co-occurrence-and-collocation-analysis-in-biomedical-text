import sys
from MHDC_Mode import ModeManager
from Utils.Decorator import timing_method

@timing_method
def main():
    """Medhelp Data Collector
    """
    try:
        mode_name = sys.argv[1].lower() if len(sys.argv) > 1 else "manual"
        mode_manager = ModeManager()
        mode_manager.run_mode(mode_name)
    except Exception as ex:
        raise ex

if __name__ == "__main__":
    main()