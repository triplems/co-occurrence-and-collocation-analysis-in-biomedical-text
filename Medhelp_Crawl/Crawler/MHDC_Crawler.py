import os
from configparser import ConfigParser
from abc import ABCMeta, abstractmethod

class MainCrawler(metaclass = ABCMeta):
    # __metaclass__ = ABCMeta

    def __init__(self,
                 base_url: str,
                 ) -> None:
        super().__init__()

        self.base_url = base_url

    @abstractmethod
    def crawl(self) -> None:
        print("Start Crawling ...")
        pass
