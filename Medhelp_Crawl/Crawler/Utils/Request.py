from Utils.Decorator import timing_method
from urllib.request import urlopen, Request as Req
from configparser import ConfigParser
from retry import retry
import contextlib
import os


CONST_CONFIG = ConfigParser(delimiters=("="))
CONST_CONFIG.read(
    os.path.join(
        os.path.dirname(
            os.path.dirname(
                os.path.dirname(__file__)
            )
        ), "Constants.ini"
    )
)

# Globals
Tries = int(CONST_CONFIG["Request"]["max_tries"])
Delay = int(CONST_CONFIG["Request"]["retry_delay"]) #Second

class Request:
    def __init__(self, tries: int = None, delay_between_retries: int = None) -> None:
        global Tries, Delay
        Tries = tries if tries else Tries
        Delay = delay_between_retries if delay_between_retries else Delay

        self.headers = {
            "User-Agent": "Mozilla/5.0"
        }
        
    def make_headers(self, **kwargs) -> None:
        self.headers = dict(kwargs)
    
    @timing_method
    @retry(exceptions=(TimeoutError),
           tries = Tries,
           delay = Delay, 
           backoff = 2
           )
    def req(self,
            url: str,
            method: str,
            timeout: float = float(CONST_CONFIG["Request"]["req_timeout"])
            ) -> bytes:
        try:
            if method not in ("GET", "POST"):
                raise ValueError(f"Invalid Method: {method}")
            
            request = Req(
                url=url,
                headers=self.headers,
                method=method
            )
            
            with contextlib.closing(urlopen(request, timeout=timeout)) as response:
                html_content = response.read()
                
        except Exception as ex:
            ex.args = (ex.args, f"Error Occurred During Request To \"{url}\"({method})")
            raise
        
        else:
            return html_content
