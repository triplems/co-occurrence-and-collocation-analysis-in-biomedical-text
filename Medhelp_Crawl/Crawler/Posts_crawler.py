from .MHDC_Crawler import MainCrawler
from .Utils.Request import Request
from Utils.Decorator import timing_method
from Scraper.Posts_Scraper import PostsScraper

class PostsCrawler(MainCrawler):

    def __init__(self,
                 base_url: str,
                 conversation_link: str
                 ) -> None:
        super().__init__(base_url)
        
        self.conversation_link = conversation_link
        
    @timing_method
    def crawl(self) -> None:
        super().crawl()
        try:
            # Fetch the conversation page
            response = Request().req(url = self.conversation_link, method = "GET")

            # Scrape content of the conversation, using the dedicated Scraper class
            post_scraper = PostsScraper(response, self.conversation_link)
            post_scraper.scrap()

        except Exception as ex:
            ex.args = (*ex.args,
                       f"Crawling Posts of the \"{self.conversation_link}\" conversation link")
            raise ex
        
        
        