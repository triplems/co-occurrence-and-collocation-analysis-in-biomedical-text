from .MHDC_Crawler import MainCrawler
from .Utils.Request import Request
from Utils.Decorator import timing_method
from Scraper.Conversations_Scraper import ConversationsScraper  

class ConversationsCrawler(MainCrawler):

    def __init__(self,
                 base_url: str,
                 community_link: str,
                 community_name: str
                 ) -> None:
        super().__init__(base_url)
        
        self.community_link = community_link
        self.community_name = community_name

    @timing_method
    def crawl(self) -> None:
        super().crawl()
        try:
            # Fetch the community page
            response = Request().req(url = self.community_link, method = "GET")

            # Scrape the content using a dedicated Scraper class
            conversation_scraper = ConversationsScraper(response,
                                                        self.community_link,
                                                        self.community_name)
            conversation_scraper.scrap()

        except Exception as ex:
            ex.args = (*ex.args,
                       f"Crawling Conversations of '{self.community_name}' community")
            raise ex