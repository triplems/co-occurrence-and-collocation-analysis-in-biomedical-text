from .MHDC_Crawler import MainCrawler
from Utils.Decorator import timing_method
from .Utils.Request import Request
from Scraper.Communities_Scraper import CommunitiesScraper

from urllib.parse import urljoin

class CommunitiesCrwaler(MainCrawler):
    
    def __init__(self,
                 base_url: str,
                 url_path: str
                 ) -> None:
       super().__init__(base_url)
       
       self.url = urljoin(self.base_url, url_path)
       
    @timing_method
    def crawl(self) -> None:
        super().crawl() 
        try:
            # Fetch the webpage
            response = Request().req(url = self.url,
                                     method = "GET",
                                     timeout = 10)
                               
            # Scrape the content using a dedicated Scraper class
            community_scraper = CommunitiesScraper(response)
            community_scraper.scrap()
            
        except Exception as ex:
            ex.args = (*ex.args,
                       "Crawling Communities")
            raise ex