import os
from configparser import ConfigParser
from bs4 import BeautifulSoup as BS
from abc import ABCMeta, abstractmethod

CONST_CONFIG = ConfigParser(delimiters=("="))
CONST_CONFIG.read(
    os.path.join(
        os.path.dirname(
            os.path.dirname(__file__)
        ), "Constants.ini"
    )
)

class MainScraper(metaclass = ABCMeta):
    
    def __init__(self, 
                 html_data: bytes,
                 base_url: str = CONST_CONFIG["Request"]["base_url"]) -> None:
        self.soup = BS(html_data, "html.parser")
        self.base_url = base_url
    
    @abstractmethod
    def scrap(self) -> None:
        print("Start Scraping ...")
        pass
        