from .MHDC_Scraper import MainScraper
from Utils.Decorator import timing_method
from Utils.Collector import VariableCollector
from Parser.Posts_Parser import PostParser

from collections import defaultdict
from urllib.parse import urljoin, urlencode
from bs4 import BeautifulSoup as BS
from typing import Tuple, List, Dict, Union
from copy import copy
import datetime
import json
import re


class PostsScraper(MainScraper):

    def __init__(self,
                 html_data: bytes,
                 conversation_link: str,
                 ) -> None:
        super().__init__(html_data)
        
        self.conversation_link = conversation_link

    @timing_method
    def scrap(self):
        """
        Extracts information about posts from the provided BeautifulSoup object.

        Returns:
            ...
        """

        super().scrap()

        try:
            post_show_content_soup = self.soup.find("div", id = "post_show_content")

            ########################################################################
            # Scrap related subject of the conversation
            question_info = dict()
            
            script_data_dict = json.loads(script_soup.get_text().strip()) \
                if (script_soup := self.soup.find("script", {"type": "application/ld+json"})) \
                else None
            subject_soup = post_show_content_soup.find("div", class_="mh_vit_card")
            
            questioner_user_info = self.scrap_conversation_question(
                copy(subject_soup), copy(script_data_dict), question_info
            )
            
            # Update just "subj_msg" and "created_timestamp"
            PostParser({
                "conversation_link": self.conversation_link,
                "question": question_info,
            }).update_question()
            
            if "type" in questioner_user_info:
                PostParser(questioner_user_info).update_user_type()
            ########################################################################
            
            ########################################################################
            # Scrap related questions of the conversation
            if (rel_ques_soup := subject_soup.find("div", class_ = re.compile(r".*rel_ques.*"))):
                related_questions_list, related_users_list = self.scrap_conversation_related_questions(
                    copy(rel_ques_soup))
                                
                # No update if exists
                PostParser(related_users_list).insert_related_users()
                
                # No update if exists
                PostParser(related_questions_list).insert_related_questions()
                
                rel_conv_links = [_d["question_info"]["conversation_link"] 
                                  for _d in related_questions_list]
                PostParser({
                    "conversation_link": self.conversation_link,
                    "rel_conv_links": rel_conv_links,
                }).insert_into_questions_relation()
            ########################################################################
              
            # Scrap all the related posts of the conversation (along with their authors)
            post_list_ctn_soup = post_show_content_soup.find("div", class_ = "post_list_ctn") 
            best_answer_card_ctn_soup = post_show_content_soup.find("div", class_ = "best_answer_card")

            posts_list, authors_list = self.scrap_conversation_posts(
                post_list_ctn_soup,
                best_answer_card_ctn_soup,
                int(re.search(r"\d+$", self.conversation_link).group())
            )

            PostParser(authors_list).insert_related_users()
            PostParser(posts_list).parse()
            

        except Exception as ex:
            ex.args = (*ex.args,
                       "Scraping posts")
            raise ex

    @timing_method
    def scrap_conversation_question(self,
                                    subject_soup: BS,
                                    script_data_dict: Dict,
                                    question_info: Dict,  # output
                                    ) -> Dict[str, Union[str, int]]:
        try:
            user_info = dict()
            
            # Extracting data from within the question's subject information (header)
            if (username := subject_soup.find("div", class_="username")):
                try:
                    a_tag = username.find("a")
                    user_info.update(VariableCollector(
                        user_id = a_tag["id"],
                        username = a_tag.text.strip(),
                        personal_page_link = urljoin(self.base_url, a_tag["href"]),
                        profile_image_link = None,
                        ).get_variables())
                    
                except TypeError:
                    sapn_tag = username.find("span", class_ = "uname")
                    clean_user_name = sapn_tag.text.strip().replace('"', '').replace("'", '')
                    
                    user_info.update(VariableCollector(
                        user_id = "external_user_" + clean_user_name,
                        username = clean_user_name,
                        personal_page_link = None,
                        profile_image_link = None,
                    ).get_variables())
                    
                    pass
            else:
                input("'user_id' Not Found ...")

            if script_data_dict:
                question_info.update(VariableCollector(
                    subj_msg = script_data_dict["description"],
                    created_timestamp = int(datetime.datetime.fromisoformat(
                        script_data_dict["mainEntity"]["dateCreated"]
                        ).timestamp()),
                    ).get_variables())

                author_info = script_data_dict["mainEntity"]["author"]
                user_info.update(VariableCollector(
                    type = author_info["@type"], # just this is a new value for insertion
                    username = author_info["name"]
                    ).get_variables())

            else:
                # Extracting data from within the question's subject message (body)
                if (subject_msg := subject_soup.find("div", id="subject_msg")):
                    question_info.update(VariableCollector(
                        subj_msg = subject_msg.get_text().strip(),
                        ).get_variables())
                
                # Extracting data from within the fetched question's subject information (header)
                if username:
                    time_tag = username.find("time")
                    question_info.update(VariableCollector(
                        created_timestamp = int(time_tag["data-timestamp"])
                        ).get_variables())
            
            return user_info

        except Exception as ex:
            ex.args = (*ex.args,
                       f"Scraping Question (Subject, User)")
            raise ex
    
    @timing_method    
    def scrap_conversation_related_questions(self,
                                             rel_ques_soup: BS,
                                             ) -> Tuple[List[dict],List[dict]]:
        related_questions, related_users = list(), list()
        try:
            for recommended_post in rel_ques_soup.findAll("div", class_ = "recommended_post"):
                q_info, u_info = dict(), dict()
                
                # Extracting the recommended subject's title data
                if (recommended_title := recommended_post.find("h2", class_ = "recommended_title")):
                    a_tag = recommended_title.find("a")
                    forum_name = re.search(r"/posts/([^/]+)/", a_tag["href"]).group(1)
                    
                    q_info.update(VariableCollector(
                        conversation_link = urljoin(self.base_url, a_tag["href"]),
                        subj_id = int(re.search(r"\d+$", a_tag["href"]).group()),
                        subj_title = a_tag.find("span").get_text().strip(),
                        ).get_variables())
                    
                # Extracting the recommended subject's user data
                if (recommended_user := recommended_post.find("div", class_ = "recommended_user")):
                    a_tag = recommended_user.find("a")
        
                    q_info.update(VariableCollector(
                        questioner_id = "user_" + re.search(r"\d+$", a_tag["href"]).group(),
                        ).get_variables())
                    
                    u_info.update(VariableCollector(
                        user_id = q_info["questioner_id"],
                        username = a_tag.text.strip(),
                        personal_page_link = urljoin(self.base_url, a_tag["href"]),
                        profile_image_link = None,
                    ).get_variables())
                
                # Extracting the recommended subject's body data (summary of the message) 
                if (recommended_body := recommended_post.find("div", class_ = "recommended_body")):
                    q_info.update(VariableCollector(
                        subj_msg = recommended_body.get_text().strip(),
                        ).get_variables())
                    
                # Additional Information
                q_info.update(
                    {
                        "forum_page_no": -1         
                    }
                )
                
                related_questions.append(
                    {
                        "question_info": q_info,
                        "forum_name": forum_name
                    }
                )
                related_users.append(u_info)
            
            return related_questions, related_users
                        
        except Exception as ex:
            ex.args = (*ex.args,
                       f"Scraping Related Questions")
            raise ex

    @timing_method
    def scrap_conversation_posts(self,
                                 post_list_ctn_soup: BS,
                                 best_answer_card_ctn_soup: BS,
                                 subject_id = int,
                                 ) -> Tuple[List[dict], List[dict]]:
        posts, users = list(), list()
        try:
            if best_answer_card_ctn_soup:
                best_answer_info, best_user_answered_info = dict(), dict()
                
                # Extracting data from within the answer's best response header
                if (resp_header := best_answer_card_ctn_soup.find("div", class_ = "resp_header")):
                    best_answer_info.update(VariableCollector(
                        post_id = int(re.search(r"\d+", resp_header["id"]).group())
                        ).get_variables())
                
                    # Extracting data from the best answerer user user_avatar
                    if (user_avatar := resp_header.find("div", class_ = "user_avatar")):
                        best_user_answered_info.update(VariableCollector(
                            profile_image_link = ("https:" + src)
                                                 if "RoR" not in (src := user_avatar.find("img")["src"])
                                                 else urljoin(self.base_url, src),
                            ).get_variables())
                    else:
                        best_user_answered_info["profile_image_link"] = None
                        
                    # Extracting data from the best answerer user resp_info
                    if (username := resp_header.find("div", class_ = "username")):
                        try:
                            a_tag = username.find("a")
                            time_tag = username.find("time")
                            
                            best_answer_info.update(VariableCollector(
                                author_id = a_tag["id"],
                                created_timestamp = int(time_tag["data-timestamp"]),
                                ).get_variables())
                            
                            best_user_answered_info.update(VariableCollector(
                                user_id = a_tag["id"],
                                username = a_tag.text.strip(),
                                personal_page_link = urljoin(self.base_url, a_tag["href"]),
                            ).get_variables())
                        
                        except TypeError:
                            sapn_tag = username.find("span", class_ = "uname")
                            time_tag = username.find("time")
                            
                            clean_user_name = sapn_tag.text.strip().replace('"', '').replace("'", '')
                            
                            best_answer_info.update(VariableCollector(
                                author_id = "external_user_" + clean_user_name,
                                created_timestamp = int(time_tag["data-timestamp"]),
                                ).get_variables())
                            
                            best_user_answered_info.update(VariableCollector(
                                user_id = best_answer_info["author_id"],
                                username = clean_user_name,
                                personal_page_link = None,
                            ).get_variables())
                            
                            pass
                    
                    # Extracting data from within the best answer's response body
                    if (comment_body := resp_header.find("div", class_ = "comment_body")):        
                        best_answer_info.update(VariableCollector(
                            post_msg = comment_body.get_text().strip(),
                            ).get_variables())
                    
                    # Additional information for the answer object
                    best_answer_info.update(
                        {
                            "subj_id": subject_id,
                            "answer_post_id": None,
                            "type": "Answer",
                            "upvote_count": 0,
                            "best_answer": "true",
                            "url": self.conversation_link + f"#post_{best_answer_info['post_id']}"
                        }
                    )
                       
                    comments, users_commented = self.scrap_conversation_comments(
                        best_answer_info['post_id'],
                        best_answer_info['url'],
                        subject_id,
                        resp_header.findAll("div", class_ = "comment_ctn")
                    )
                
                    posts.append(best_answer_info)
                    posts.extend(comments)
                    users.append(best_user_answered_info)
                    users.extend(users_commented)
                
            for post_ctn in post_list_ctn_soup.findAll("div", class_ = "mh_vit_resp_ctn"):
                answer_info, user_answered_info = dict(), dict()
                    
                # Extracting data from within the answer's response header
                if (resp_header := post_ctn.find("div", class_ = "resp_header")):
                    answer_info.update(VariableCollector(
                        post_id = int(re.search(r"\d+", resp_header["id"]).group())
                        ).get_variables())
                    
                    # Extracting data from the answerer user user_avatar
                    if (user_avatar := resp_header.find("div", class_ = "user_avatar")):
                        user_answered_info.update(VariableCollector(
                            profile_image_link = ("https:" + src)
                                               if "RoR" not in (src := user_avatar.find("img")["src"])
                                               else urljoin(self.base_url, src),
                            ).get_variables())
                    else:
                        user_answered_info["profile_image_link"] = None
                        
                    # Extracting data from the answerer user resp_info
                    if (username := resp_header.find("div", class_ = "username")):
                        try:
                            a_tag = username.find("a")
                            time_tag = username.find("time")
                            
                            answer_info.update(VariableCollector(
                                author_id = a_tag["id"],
                                created_timestamp = int(time_tag["data-timestamp"]),
                                ).get_variables())
                            
                            user_answered_info.update(VariableCollector(
                                user_id = a_tag["id"],
                                username = a_tag.text.strip(),
                                personal_page_link = urljoin(self.base_url, a_tag["href"]),
                            ).get_variables())
                            
                        except TypeError:
                            sapn_tag = username.find("span", class_ = "uname")
                            time_tag = username.find("time")
                            
                            clean_user_name = sapn_tag.text.strip().replace('"', '').replace("'", '')
                            
                            answer_info.update(VariableCollector(
                                author_id = "external_user_" + clean_user_name,
                                created_timestamp = int(time_tag["data-timestamp"]),
                                ).get_variables())
                            
                            user_answered_info.update(VariableCollector(
                                user_id = answer_info["author_id"],
                                username = clean_user_name,
                                personal_page_link = None,
                            ).get_variables())
                            
                            pass
                
                # Extracting data from within the answer's response body
                if (resp_body := post_ctn.find("div", class_ = "resp_body")):        
                    answer_info.update(VariableCollector(
                        post_msg = resp_body.get_text().strip(),
                        ).get_variables())
                    
                # Extracting data from within the answer's user rating content
                if (user_rating_ctn := post_ctn.find("div", class_ = "user_rating_ctn")):
                    user_rating_count_span = user_rating_ctn.find("span", id = re.compile(r"user_rating_count.*"))       
                    answer_info.update(VariableCollector(
                        upvote_count = int(user_rating_count_span.text.strip()),
                        ).get_variables())
                
                # Additional information for the answer object
                answer_info.update(
                    {
                        "subj_id": subject_id,
                        "answer_post_id": None,
                        "type": "Answer",
                        "best_answer": "false",
                        "url": self.conversation_link + f"#post_{answer_info['post_id']}"
                    }
                )
                
                comments, users_commented = self.scrap_conversation_comments(
                    answer_info['post_id'],
                    answer_info['url'],
                    subject_id,
                    post_ctn.findAll("div", class_ = "comment_ctn")
                )
                
                posts.append(answer_info)
                posts.extend(comments)
                users.append(user_answered_info)
                users.extend(users_commented)
                
            return posts, users
                    
        except Exception as ex:
            ex.args = (*ex.args,
                       f"Scraping all Posts (Answers & Comments)")
            raise ex
        
    @timing_method
    def scrap_conversation_comments(self,
                                     answer_post_id: int,
                                     answer_post_link: str,
                                     subject_id,
                                     comment_list: List[BS],
                                     ) -> Tuple[List[dict], List[dict]]:
        comments, users = list(), list()
        try:
            for comment_ctn in comment_list:
                comment_info, user_commented_info = dict(), dict()
                
                # Extracting data from the comment content
                comment_info.update(VariableCollector(
                    post_id = int(re.search(r"\d+", comment_ctn["id"]).group())
                    ).get_variables())
                    
                # Extracting data from within the comment's username class
                if (username := comment_ctn.find("div", class_ = "username")):
                    try:
                        a_tag = username.find("a")
                        time_tag = username.find("time")
                        
                        comment_info.update(VariableCollector(
                            author_id = a_tag["id"],
                            created_timestamp = int(time_tag["data-timestamp"]),
                            ).get_variables())
                        
                        user_commented_info.update(VariableCollector(
                            user_id = a_tag["id"],
                            username = a_tag.text.strip(),
                            personal_page_link = urljoin(self.base_url, a_tag["href"]),
                            profile_image_link = None,
                        ).get_variables())
                        
                    except TypeError:
                        sapn_tag = username.find("span", class_ = "uname")
                        time_tag = username.find("time")
                        
                        clean_user_name = sapn_tag.text.strip().replace('"', '').replace("'", '')
                        
                        comment_info.update(VariableCollector(
                            author_id = "external_user_" + clean_user_name,
                            created_timestamp = int(time_tag["data-timestamp"]),
                            ).get_variables())
                        
                        user_commented_info.update(VariableCollector(
                            user_id = comment_info["author_id"],
                            username = clean_user_name,
                            personal_page_link = None,
                            profile_image_link = None,
                        ).get_variables())
                        
                        pass
                    
                # Extracting data from within the comment's body
                if (comment_body := comment_ctn.find("div", class_ = "comment_body")):        
                    comment_info.update(VariableCollector(
                        post_msg = comment_body.get_text().strip(),
                        ).get_variables())
                    
                # Additional information for the comment object 
                comment_info.update(
                    {
                        "subj_id": subject_id,
                        "answer_post_id": answer_post_id,
                        "type": "Comment",
                        "upvote_count": None,
                        "best_answer": None,
                        "url": self.conversation_link + f"#post_{comment_info['post_id']}"
                    }
                )
                
                comments.append(comment_info)
                users.append(user_commented_info)
            
            return comments, users
               
        except Exception as ex:
            ex.args = (*ex.args,
                       f"Scraping all Comments of the \"{answer_post_link}\" answer post link")
            raise ex