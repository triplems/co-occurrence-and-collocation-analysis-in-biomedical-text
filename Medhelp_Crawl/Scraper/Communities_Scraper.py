from .MHDC_Scraper import MainScraper
from Utils.Decorator import timing_method
from Utils.Collector import VariableCollector
from Utils.Exception import ScrapingError
from Parser.Communities_Parser import CommunitiesParser
from Parser.Utils.Text_Cleaner import TextCleaner

from collections import defaultdict
from urllib.parse import urljoin


class CommunitiesScraper(MainScraper):

    def __init__(self, html_data: bytes) -> None:
        super().__init__(html_data)

    @timing_method
    def scrap(self):
        """
        Extracts information about communities from the provided BeautifulSoup object.

        Returns:
            A defaultdict mapping forum group titles to lists of dictionaries containing community name and link.
        """

        super().scrap()
        communities = defaultdict(list)

        try:
            # Find the container element for forum groups
            forums = self.soup.find("div", {"id": "tier_one_forums"})
            if not forums:
                raise ScrapingError("Forum groups element not found")

            for forum_group in forums.findAll("div", {"class": "forum_group"}):
                forum_group_title = forum_group.find(
                    "div", {"class", "forum_group_title"}).findNext(string=True)
                if not forum_group_title:
                    raise ScrapingError("Forum group title not found")
                cleaner = TextCleaner(forum_group_title)
                forum_group_title = cleaner.apply_filters(["strip_whitespace"])

                for forum_link in forum_group.findAll("div", {"class": "forums_link"}):
                    tag = forum_link.find("a")
                    community_info = VariableCollector(
                        name=TextCleaner(tag.contents).join_strings(),
                        link=urljoin(self.base_url, tag["href"]),
                    ).get_variables()
                    communities[forum_group_title].append(community_info)

            community_parser = CommunitiesParser(communities)
            community_parser.parse()

        except Exception as ex:
            ex.args = (*ex.args,
                       "Scraping Communities")
            raise ex
