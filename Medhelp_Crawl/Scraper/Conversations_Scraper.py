from .MHDC_Scraper import MainScraper
from Crawler.Utils.Request import Request
from Utils.Decorator import timing_method
from Utils.Collector import VariableCollector
from Utils.Exception import InvalidCommunityPageError
from Parser.Conversations_Parser import ConversationsParser

from collections import defaultdict
from urllib.parse import urljoin, urlencode, ParseResult
from bs4 import BeautifulSoup as BS
from bs4 import BeautifulSoup
from copy import copy
import time, re
from random import randint

class ConversationsScraper(MainScraper):
    
    def __init__(self, 
                 html_data: bytes,
                 community_link: str,
                 community_name: str
                 ) -> None:
        super().__init__(html_data)
        
        self.community_link = community_link
        self.community_name = community_name
        
        self.max_questions_pages = None
        self.questions_data = {}
        self.users_data = {}
        self.resources_data = []
        
    @timing_method
    def scrap(self):
        """
        Extracts information about conversations from the provided BeautifulSoup object.

        Returns:
            ...
        """
        
        super().scrap()
            
        try:
            # Scrap all related resources of the community
            for div in self.soup.find_all('div', class_ = 'link_tab'):
                if (a := div.find("a", string = "Resources")):
                    resources_link = urljoin(self.base_url, a["href"])
                    break
            resources_page_response = Request().req(url = resources_link, 
                                                    method = "GET")
            resources_soup = BS(resources_page_response, "html.parser")
            self.resources_data = self.scrap_community_resources(resources_soup)
            ConversationsParser({
                "community_link": self.community_link,
                "resources": self.resources_data,
            }).parse_community_resources()
            
            try:
                # Scrap all related questions of the community
                questions_ctn_soup = self.soup.find("div", class_ = "subject_list_ctn")
                self.scrap_community_questions_and_users(copy(questions_ctn_soup))
                
            except InvalidCommunityPageError as ex:
                self._assign_exception_data(ex)
                pass
            
            conversation_parser = ConversationsParser({
                "community_link": self.community_link,
                "questions": dict(self.questions_data),
                "users": dict(self.users_data),
                "max_pages": self.max_questions_pages,
            })
            conversation_parser.parse()
            
        except Exception as ex:
            ex.args = (*ex.args, 
                       "Scraping conversations")
            raise ex    
        
    @timing_method
    def scrap_community_questions_and_users(self, questions_soup: BeautifulSoup) -> None:
        try:
            questions, users = defaultdict(list), defaultdict(list)
            page_no = 1
            
            while(True):
                if not (subj_entries := questions_soup.findAll("div", class_ = "subj_entry")):
                    raise InvalidCommunityPageError(message = "",
                                                    max_pages = page_no - 1,
                                                    questions_data = questions,
                                                    users_data = users,)
                    
                for subj_entry in subj_entries:
                    user_info, question_info = dict(), dict()
                    ########################################################################
                    # Extracting the subject header data
                    if (user_avatar := subj_entry.find("div", class_ = "user_avatar")):
                        user_info.update(VariableCollector(
                            profile_image_link = ("https:" + src)
                                                 if "RoR" not in (src := user_avatar.find("img", {"class": "photo_link"})["src"])
                                                 else urljoin(self.base_url, src),
                            ).get_variables())
                        
                    if (subj_title := subj_entry.find("h2", class_ = "subj_title")):
                        a_tag = subj_title.find("a")
                        
                        question_info.update(VariableCollector(
                            conversation_link = urljoin(self.base_url, a_tag["href"]),
                            subj_id = int(re.search(r"\d+$", a_tag["href"]).group()),
                            subj_title = a_tag.get_text(),
                            ).get_variables())
                        
                    if (username := subj_entry.find("div", class_ = "username")):
                        try:
                            a_tag = username.find("a")
                
                            question_info.update(VariableCollector(
                                questioner_id = a_tag["id"],
                                ).get_variables())
                            
                            user_info.update(VariableCollector(
                                user_id = a_tag["id"],
                                username = a_tag.text.strip(),
                                personal_page_link = urljoin(self.base_url, a_tag["href"]),
                            ).get_variables())
                            
                        except TypeError:
                            sapn_tag = username.find("span", class_ = "uname")
                            clean_user_name = sapn_tag.text.strip().replace('"', '').replace("'", '')
                            
                            # question_info.update(VariableCollector(
                            #     questioner_id = "external_user_" + 
                            #     next(iter([n.zfill(8) for n in re.findall(r'\d+', clean_user_name)]),
                            #          clean_user_name),
                            #     ).get_variables())
                            
                            question_info.update(VariableCollector(
                                questioner_id = "external_user_" + clean_user_name,
                                ).get_variables())
                            
                            user_info.update(VariableCollector(
                                user_id = question_info["questioner_id"],
                                username = clean_user_name,
                                personal_page_link = None,
                            ).get_variables())
                            
                            pass
                    ########################################################################
                       
                    # Extracting the subject body data (summary of the message)
                    if (subject_msg := subj_entry.find("div", id = "subject_msg")):
                        question_info.update(VariableCollector(
                            subj_msg = subject_msg.get_text(),
                            ).get_variables())
                        
                    # Extracting the subject stats data
                    if (subj_stats := subj_entry.find("div", class_ = "subj_stats")):
                        # timestamp_sec = subj_stats.find("span", class_ = "mh_timestamp")
                        answer_span = subj_stats.find("span", string = re.compile("answer"))
                        comment_span = subj_stats.find("span", string = re.compile("comment"))
                        
                        question_info.update(VariableCollector(
                            # last_action_timestamp = int(timestamp_sec["data-timestamp"]),
                            answer_count = int(re.match(r"\d+", answer_span.text).group())
                                           if answer_span
                                           else 0,
                            comment_count = int(re.match(r"\d+", comment_span.text).group()) 
                                            if comment_span
                                            else 0,
                            ).get_variables())
                     
                    # Additional Information
                    question_info.update(
                        {
                            "forum_page_no": page_no         
                        }
                    )
                        
                    questions[page_no].append(question_info)
                    users[page_no].append(user_info)
                 
                if not page_no % 10:
                    conversation_parser = ConversationsParser({
                        "community_link": self.community_link,
                        "questions": dict(questions),
                        "users": dict(users),
                        "max_pages": page_no,
                    })
                    conversation_parser.parse()
                    questions, users = defaultdict(list), defaultdict(list)
                    time.sleep(7)
                    
                page_no += 1
                # Fetch the community with specific page number 
                page_response = Request().req(url = self.community_link +
                                              '?' + 
                                              urlencode({"page": page_no}), 
                                              method = "GET")
                questions_soup = BS(page_response, 'html.parser').find("div", class_ = "subject_list_ctn")
        
        except Exception as ex:
            ex.args = (*ex.args,
                       f"Scraping Questions and Users of '{self.community_name}' community")
            raise ex
            
    @timing_method
    def scrap_community_resources(self, resources_soup: BeautifulSoup) -> list:
        try:
            resources = list()
            resources_ctn_soup = resources_soup.find("div", class_ = "topic_ctn")
            
            for article_ctn in resources_ctn_soup.findAll("div", class_ = "article_ctn"):
                article_info = dict()
                
                if (article_img := article_ctn.find("div", {"class": "article_img"})):
                    article_info.update(VariableCollector(
                        article_img_link = ("https:" + src)
                                           if "RoR" not in (src := article_img.find("img")["src"])
                                           else urljoin(self.base_url, src),
                        ).get_variables())
                    
                if (article_title := article_ctn.find("div", {"class": "article_title"})):
                    a_tag = article_title.find("a")
                    article_info.update(VariableCollector(
                        article_title = a_tag.text,
                        article_link = urljoin(self.base_url, a_tag["href"]),
                        article_desc = article_ctn.find("div", class_ = "article_desc").get_text(),
                        ).get_variables())
                
                resources.append(article_info)
                
            return resources
        
        except Exception as ex:
            ex.args = (*ex.args, 
                       f"Scraping Resources of '{self.community_name}' community")
            raise ex
        
    def _assign_exception_data(self, ex: InvalidCommunityPageError):
        """Assigns exception data to instance variables."""
        
        self.max_questions_pages = ex.max_page
        self.questions_data = ex.questions_data
        self.users_data = ex.users_data
        
